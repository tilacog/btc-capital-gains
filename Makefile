lint:
	pylama --linters mccabe,pep257,pydocstyle,pep8,pycodestyle,pyflakes,mypy

.PHONY = lint
