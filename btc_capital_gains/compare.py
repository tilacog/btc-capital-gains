from dataclasses import dataclass
from datetime import datetime
from decimal import Decimal

from api_requests.historical import Candle
from api_requests.unspent import Balance


@dataclass
class Comparison:
    btc_amount: Decimal
    previous_date: datetime
    previous_usd_price: Decimal  # per 1 BTC
    current_usd_price: Decimal  # per 1 BTC

    @property
    def previous_value(self):
        return self.btc_amount * self.previous_usd_price

    @property
    def current_value(self):
        return self.btc_amount * self.current_usd_price

    @property
    def gains_or_losses(self):
        return self.current_value - self.previous_value


def compare(current_price: Decimal, balance: Balance, historical: Candle):
    return Comparison(
        btc_amount=balance.amount,
        previous_date=balance.date,
        previous_usd_price=historical.close,
        current_usd_price=current_price,
    )
