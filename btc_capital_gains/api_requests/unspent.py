from dataclasses import dataclass
from datetime import datetime
from decimal import Decimal
from typing import List

import requests


@dataclass
class Balance:
    date: datetime
    amount: Decimal


ADDRESS_API = "https://blockchain.info/rawaddr/"


def query_for_balances(address: str) -> dict:
    url = ADDRESS_API + address
    response = requests.get(url)
    response.raise_for_status()
    return response.json()


def parse_transaction_object(json_object: dict, address: str) -> Balance:
    date = datetime.fromtimestamp(json_object["time"])
    satoshis = sum(
        out["value"]
        for out in json_object["out"]
        if out["addr"] == address
        if not out["spent"]
    )
    amount = Decimal(satoshis / 1e08)
    return Balance(date, amount)


def get_balances_for_address(address: str) -> List[Balance]:
    json = query_for_balances(address)
    return [
        parse_transaction_object(transaction, address)
        for transaction in json["txs"]
    ]


if __name__ == "__main__":
    import sys

    total = Decimal("0")
    addr = sys.argv[1]
    for i in get_balances_for_address(addr):
        print(i)
        total += i.amount
    print(f"Total: {total}")
