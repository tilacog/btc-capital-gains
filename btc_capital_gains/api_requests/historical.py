from dataclasses import dataclass
from datetime import datetime, timedelta
from decimal import Decimal
from typing import Tuple

import requests


@dataclass
class Candle:
    "Represents a candlestick for a historical quotation"
    time_open: datetime
    time_close: datetime
    open_: Decimal
    high: Decimal
    low: Decimal
    close: Decimal


API_DATETIME_FMT = "%Y-%m-%dT%H:%M:%S.%fZ"
HISTORICAL_PRICE_API = (
    "https://web-api.coinmarketcap.com/v1/cryptocurrency/ohlcv/historical"
)


def build_date_interval(date: datetime) -> Tuple[int, int]:
    """
    Prepare date interval for interacting with CoinMarketCap API.
    """
    # truncate base date before operations
    base_date = date.replace(hour=0, minute=0, second=0, microsecond=0)
    one_day = timedelta(days=1)
    one_second = timedelta(seconds=1)
    day_before = base_date - one_day
    day_after = base_date + one_day - one_second

    # convert to integer timestamp
    ts_before = int(day_before.timestamp())
    ts_after = int(day_after.timestamp())

    return (ts_before, ts_after)


def query_for_historical_price(date: datetime) -> dict:
    start, end = build_date_interval(date)
    response = requests.get(
        HISTORICAL_PRICE_API,
        params=dict(
            time_start=start, time_end=end, convert="USD", slug="bitcoin",
        ),
    )
    response.raise_for_status()
    return response.json()


def parse_json(json_object: dict) -> Candle:
    data = json_object["data"]
    quote = data["quotes"][0]
    usd = quote["quote"]["USD"]
    return Candle(
        time_open=datetime.strptime(quote["time_open"], API_DATETIME_FMT),
        time_close=datetime.strptime(quote["time_close"], API_DATETIME_FMT),
        open_=Decimal(usd["open"]),
        high=Decimal(usd["high"]),
        low=Decimal(usd["low"]),
        close=Decimal(usd["close"]),
    )


def get_historical_price(date: datetime) -> Candle:
    historical_price = query_for_historical_price(date)
    return parse_json(historical_price)


if __name__ == "__main__":
    from pprint import pprint

    someday = datetime.now()
    print(f"target time: {someday.isoformat()}")

    date_interval = build_date_interval(someday)
    print(f"date_interval = {date_interval}")
    historical_price = query_for_historical_price(someday)
    pprint(historical_price)
    print(parse_json(historical_price))
