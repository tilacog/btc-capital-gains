from decimal import Decimal

from pyquery import PyQuery

URL = "https://coinmarketcap.com/currencies/bitcoin/"
CSS_SELECTOR = ".cmc-details-panel-price__price"


def query_current_price() -> str:
    document = PyQuery(url=URL)
    return document(CSS_SELECTOR).text()


def parse_price(text: str) -> Decimal:
    return Decimal(text.replace("$", "").replace(",", ""))


def get_current_price() -> Decimal:
    as_text = query_current_price()
    return parse_price(as_text)


if __name__ == "__main__":
    current_price_as_text = query_current_price()
    print(f"current price as text: {current_price_as_text}")

    parsed_price = parse_price(current_price_as_text)
    print(f"parsed price: {parsed_price}")
