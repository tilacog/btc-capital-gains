import sys
from concurrent.futures import ThreadPoolExecutor
from dataclasses import dataclass
from decimal import Decimal
from typing import List

from api_requests.historical import get_historical_price
from api_requests.price import get_current_price
from api_requests.unspent import get_balances_for_address
from compare import Comparison, compare
from tabulate import tabulate


@dataclass
class Accumulator:
    capital_gains: Decimal = Decimal(0)
    balance_usd: Decimal = Decimal(0)
    balance_btc: Decimal = Decimal(0)

    def __add__(self, other) -> "Accumulator":
        return Accumulator(
            capital_gains=self.capital_gains + other.capital_gains,
            balance_usd=self.balance_usd + other.balance_usd,
            balance_btc=self.balance_btc + other.balance_btc,
        )


def fetch_data(address: str) -> List[Comparison]:
    with ThreadPoolExecutor() as executor:
        current_price = executor.submit(get_current_price)
        balances = executor.submit(get_balances_for_address, address)
        current_price = current_price.result()
        balances = balances.result()
        historical_values = executor.map(
            get_historical_price, [b.date for b in balances]
        )

    return [
        compare(current_price, balance, historical)
        for (balance, historical) in zip(balances, historical_values)
    ]


def produce_rows(address: str, accumulator):
    # header
    yield [
        "#",
        "Date",
        "BTC",
        "USD Price (historical)",
        "USD Amount (historical)",
        "USD Price (current)",
        "USD Amount (current)",
        "Gains(+)/Losses(-)",
    ]
    comparisons = fetch_data(address)
    iterable = sorted(comparisons, key=lambda comp: comp.previous_date)
    for index, comp in enumerate(iterable, start=1):
        accumulator.capital_gains += comp.gains_or_losses
        accumulator.balance_usd += comp.current_value
        accumulator.balance_btc += comp.btc_amount
        yield (
            index,
            comp.previous_date.date(),
            comp.btc_amount,
            comp.previous_usd_price,
            comp.previous_value,
            comp.current_usd_price,
            comp.current_value,
            comp.gains_or_losses,
        )


def main(address) -> Accumulator:
    print(f"Address: {address}")
    accumulator = Accumulator()
    generator = produce_rows(address, accumulator)
    print(tabulate(generator, headers="firstrow", floatfmt=".4f"))
    print(f"Capital Gains (USD)  : {accumulator.capital_gains:.2f}")
    print(f"Address Balance (USD): {accumulator.balance_usd:.2f}")
    print(f"Address Balance (BTC): {accumulator.balance_btc:.8f}")

    return accumulator


if __name__ == "__main__":
    addresses = sys.argv[1:]
    accumulator = Accumulator()
    for address in addresses:
        accumulator += main(address)
        print()

    if len(addresses) > 1:
        print("-----")
        print(f"Total Capital Gains (USD)  : {accumulator.capital_gains:.2f}")
        print(f"Total Balance (USD)        : {accumulator.balance_usd:.2f}")
        print(f"Total Balance (BTC)        : {accumulator.balance_btc:.8f}")
