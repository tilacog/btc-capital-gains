# btc-capital-gains

## usage

```sh
$ poetry run btc_capital_gains/main.py [bitcoin-address] [...other addressses]
```

## TODO
- assign an executable script _(`[tool.poetry.scripts]`)_
- ship to pypi
- format btc amount with 8 decimal places, fiat with 2
- cache historical data _(txns, historical prices, ...)_
- better CLI _(use click or something else)_
- better APIs?
